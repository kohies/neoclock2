#include <FastLED.h>
#include <DS3231.h>
#include <Wire.h>

#define NUM_LEDS 77
#define DATA_PIN 3
#define BUTTON1_PIN 11
#define BUTTON2_PIN 12
#define ROW_HEIGHT 11
#define ANIMATION_SPEED 20

DS3231 Clock;
bool h12Flag = false;
bool pmFlag = false;
int hour;
int adjustedHour;
int minute;
int second;
boolean summertime;
bool Century = false;

byte buttonState;
byte previousButtonState = 0b011000;
unsigned long currentMillis = 0;
unsigned long previousMillis = 0;
unsigned long holdOff = 50;

unsigned long weekdayLedInterval = 250;
unsigned long weekdayLedPause = 2000;
unsigned long weekdayIntervalPreviousMillis;
byte weekdayCounter;
bool weekdayLedState = 0;
bool weekdayBlinkPaused = 0;
unsigned long weekdayPausePreviousMillis;

CRGB leds[NUM_LEDS];
uint16_t globalHue = 160;
uint8_t globalHueDelta = 1;
uint16_t hourHue = 140;
uint16_t minuteHue = 180;
uint16_t secondHue = 220;

int getHueForRow(int rowNumber)
{
  switch (rowNumber)
  {
  case 1:
    return hourHue;
  case 2:
    return hourHue;
  case 3:
    return minuteHue;
  case 4:
    return minuteHue;
  case 6:
    return secondHue;
  case 7:
    return secondHue;
  }
  return globalHue;
}

void showTime(int hour, int minute, int second)
{
  FastLED.clear();
  for (int i = 0; i < hour / 10; i++)
  {
    leds[i].setHue(hourHue);
  }
  for (int i = 11; i < 11 + hour % 10; i++)
  {
    leds[i].setHue(hourHue);
  }
  for (int i = 22; i < 22 + minute / 10; i++)
  {
    leds[i].setHue(minuteHue);
  }
  for (int i = 33; i < 33 + minute % 10; i++)
  {
    leds[i].setHue(minuteHue);
  }
  for (int i = 55; i < 55 + second / 10; i++)
  {
    leds[i].setHue(secondHue);
  }
  for (int i = 66; i < 66 + second % 10; i++)
  {
    leds[i].setHue(secondHue);
  }
  FastLED.show();
}

boolean isSummertime(int year, int month, int day, int hour, int tzHours)
{
  if (month < 3 || month > 10)
    return false; // keine Sommerzeit in Jan, Feb, Nov, Dez
  if (month > 3 && month < 10)
    return true; // Sommerzeit in Apr, Mai, Jun, Jul, Aug, Sep
  if (month == 3 && (hour + 24 * day) >= (1 + tzHours + 24 * (31 - (5 * year / 4 + 4) % 7)) || month == 10 && (hour + 24 * day) < (1 + tzHours + 24 * (31 - (5 * year / 4 + 1) % 7)))
    return true;
  else
    return false;
}

void dropIn(int row, int maxNumber)
{
  if (leds[(row - 1) * ROW_HEIGHT + maxNumber - 1])
  {
    return;
  }
  int i = row * ROW_HEIGHT - 1;
  while (!leds[i] && i >= (row - 1) * ROW_HEIGHT)
  {
    leds[i].setHue(getHueForRow(row));
    FastLED.show();
    FastLED.delay(ANIMATION_SPEED);
    if (!leds[i - 1] && i > (row - 1) * ROW_HEIGHT)
    {
      leds[i] = CRGB::Black;
      FastLED.show();
    }
    i--;
  }
}

void emptyOut(int row, int maxNumber)
{
  for (int i = (row - 1) * ROW_HEIGHT + maxNumber - 1; i >= (row - 1) * ROW_HEIGHT; i--)
  {
    leds[i] = CRGB::Black;
    FastLED.show();
    FastLED.delay(ANIMATION_SPEED);
  }
}

void weekDayBlink()
{
  if (weekdayCounter == 0 && weekdayLedState)
  {
    if (millis() - weekdayIntervalPreviousMillis > weekdayLedInterval)
    {
      digitalWrite(LED_BUILTIN, 0);
    }
    weekdayBlinkPaused = true;
    if (millis() - weekdayPausePreviousMillis > weekdayLedPause)
    {
      // Serial.println("Pause vorbei");
      weekdayBlinkPaused = false;
      weekdayPausePreviousMillis = millis();
    }
  }

  if (!weekdayBlinkPaused)
  {
    if (millis() - weekdayIntervalPreviousMillis > weekdayLedInterval || weekdayCounter == 0)
    {
      // Serial.println("blink intervall abgelaufen");
      {
        weekdayLedState = !weekdayLedState;
        if (weekdayLedState)
        {
          weekdayCounter = (weekdayCounter + 1) % Clock.getDoW();
          // Serial.println(weekdayCounter, DEC);
        }
        digitalWrite(LED_BUILTIN, weekdayLedState);
      }
      weekdayIntervalPreviousMillis = millis();
      weekdayPausePreviousMillis = millis();
    }
  }
}

void setup()
{
  Wire.begin();
  Serial.begin(9600);

  pinMode(BUTTON1_PIN, INPUT_PULLUP);
  pinMode(BUTTON2_PIN, INPUT_PULLUP);
  buttonState = PINB;
  previousMillis = millis();

  pinMode(LED_BUILTIN, OUTPUT);
  weekdayCounter = 1;
  weekdayIntervalPreviousMillis = millis();
  weekdayPausePreviousMillis = millis();

  //  Clock.setClockMode(false);
  //  Clock.setYear(20);
  //  Clock.setMonth(12);
  //  Clock.setDate(2);
  //  Clock.setHour(13);
  //  Clock.setMinute(5);
  //  Clock.setSecond(50);
  //  Clock.setDoW(5);

  second = Clock.getSecond();
  minute = Clock.getMinute();
  hour = Clock.getHour(h12Flag, pmFlag);
  summertime = isSummertime(Clock.getYear() + 2000, Clock.getMonth(Century), Clock.getDate(), hour, 1);
  adjustedHour = summertime ? hour + 1 : hour;

  FastLED.setBrightness(15);
  FastLED.addLeds<NEOPIXEL, DATA_PIN>(leds, NUM_LEDS);

  showTime(adjustedHour, minute, second);
}

void loop()
{
  byte reading = PINB;
  if (reading != previousButtonState)
  {
    previousMillis = millis();
  }
  if (millis() - previousMillis > holdOff)
  {
    if (reading != buttonState)
    {
      buttonState = reading;

      switch (buttonState)
      {
      case 0b010000:
        hour = (hour + 1) % 24;
        Clock.setHour(hour);
        adjustedHour = summertime ? hour + 1 : hour;
        showTime(adjustedHour, minute, second);
        break;
      case 0b001000:
        minute = (minute + 1) % 60;
        Clock.setMinute(minute);
        showTime(adjustedHour, minute, second);
        break;
      case 0b000000:
        globalHueDelta = (globalHueDelta + 1) % 6;
        break;
      }
    }
  }
  previousButtonState = reading;

  if (Clock.getSecond() % 10 != second % 10)
  {
    dropIn(7, 9);
    if (second % 10 == 9)
    {
      emptyOut(7, 9);
      dropIn(6, 5);
      if (second / 10 == 5)
      {
        emptyOut(6, 5);
        dropIn(4, 9);
        if (minute % 10 == 9)
        {
          emptyOut(4, 9);
          dropIn(3, 5);
          if (minute / 10 == 5)
          {
            emptyOut(3, 5);
            if (adjustedHour == 23)
            {
              emptyOut(2, 3);
              emptyOut(1, 2);
            }
            else
            {
              dropIn(2, 9);
              if (adjustedHour % 10 == 9)
              {
                emptyOut(2, 9);
                dropIn(1, 2);
              }
            }
          }
        }
      }
    }
    summertime = isSummertime(Clock.getYear() + 2000, Clock.getMonth(Century), Clock.getDate(), hour, 1);
    second = Clock.getSecond();
    minute = Clock.getMinute();
    hour = Clock.getHour(h12Flag, pmFlag);
    adjustedHour = summertime ? hour + 1 : hour;
  }

  hourHue += globalHueDelta;
  minuteHue += globalHueDelta;
  secondHue += globalHueDelta;
  showTime(adjustedHour, minute, second);
  FastLED.delay(50);
}
